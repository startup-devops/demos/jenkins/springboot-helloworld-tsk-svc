FROM gitlab.dev-cehd.devsecops-cibanco.com:5050/cehd/containers-images/spring-boot-image/http-service-layer:1.0.0.RELEASE
ARG JAR_FILE

COPY ${JAR_FILE} ${HTTP_SERVICE_HOME}/${HTTP_SERVICE_ARTIFACT_NAME}
