variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_REPOSITORY: "$CI_PROJECT_DIR/.m2/repository"
  MAVEN_OPTS: >-
    -Dhttps.protocols=TLSv1.2 
    -Dmaven.repo.local=$MAVEN_REPOSITORY 
    -Dorg.slf4j.simpleLogger.showDateTime=true 
    -Djava.awt.headless=true
  
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: >-
    --batch-mode 
    --errors 
    --fail-at-end 
    --show-version
    --no-transfer-progress
    -DinstallAtEnd=true 
    -DdeployAtEnd=true

  SONAR_USER_HOME: "$CI_PROJECT_DIR/.sonar"  # Defines the location of the analysis task cache

  DEPLOYMENT_NAME: "$CI_PROJECT_NAME"
  
  DEV_KUBECTL_CONTEXT: "cehd/mspoc/kubernetes-agents:dev-qa"
  QA_KUBECTL_CONTEXT: "cehd/mspoc/kubernetes-agents:dev-qa"
  PROD_KUBECTL_CONTEXT: "cehd/mspoc/kubernetes-agents:prod"
  
  CONTAINER_SHA_IMAGE_TAG: "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHA" # Define image tag only for DEV
  CONTAINER_IMAGE_TAG: "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" # Define image tag for QA, PROD

  DEV_NAMESPACE: "mspoc-dev-ns"  # Defines the location of the deployment
  DEV_HOST_NAME: "dev.mspoc.cibanco.app" # Define host to deploy
  DEV_TLS_SECRET_NAME: "mspoc-dev-tls" # Define tls certificate

  QA_NAMESPACE: "mspoc-qa-ns"  # Defines the location of the deployment
  QA_HOST_NAME: "qa.mspoc.cibanco.app" # Define host to deploy
  QA_TLS_SECRET_NAME: "mspoc-qa-tls" # Define tls certificate

# ------------------------------------------------------------------------
# STAGES
# ------------------------------------------------------------------------
stages:
  - init
  - build
  - code-quality
  - test
  - deploy-package
  - deploy-image-container
  - deploy-dev
  - dast-dev
  - clean-cache
  - retag-image-container
  - deploy-qa
  - undo-qa
  #- deploy-prod
  
# ------------------------------------------------------------------------
# TEMPLATES
# ------------------------------------------------------------------------
.job_maven_template: &job_maven_config 
  image: maven:3-openjdk-11
  cache:
    key: "$CI_PROJECT_NAME"
    paths:
      - "$MAVEN_REPOSITORY"
      - "./target"
  rules:
    - !reference [.dev_rules, rules]
  environment: DEV
# ------------------------------------------------------------------------
# ENVIRONMENT RULES
# ------------------------------------------------------------------------
.dev_rules:
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH)
    #- if: ($CI_COMMIT_BRANCH == "dev")

.qa_rules:
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+\-SNAPSHOT$/ # Example v1.0.0-SNAPSHOT

.prod_rules:
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+\.RELEASE$/ # Example v1.0.0.RELEASE

# ------------------------------------------------------------------------
# DEV
# ------------------------------------------------------------------------
maven-init:
  << : *job_maven_config
  stage: init
  script: 
    - "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS clean -s ci_settings.xml -Dmaven.test.skip=true"

maven-package:
  << : *job_maven_config
  stage: build
  script: 
    - "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS package -s ci_settings.xml -Dmaven.test.skip=true"
  dependencies:
    - maven-init

sonarqube-check:
  << : *job_maven_config
  stage: code-quality
  variables:
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .sonar/cache
  script:
    - "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS verify sonar:sonar -s ci_settings.xml -Dmaven.test.skip=true -Pgitlab-cicd -Dsonar.projectKey=$DEPLOYMENT_NAME -Dsonar.projectName=$DEPLOYMENT_NAME"
  allow_failure: false
  dependencies:
    - maven-package

maven-unit-test:
  << : *job_maven_config
  stage: test
  script: 
    #- "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS surefire:test"
    - "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS test -s ci_settings.xml" 
  artifacts:
    reports:
      junit:
        - ./target/surefire-reports/TEST-*.xml
        - ./target/failsafe-reports/TEST-*.xml
  dependencies:
    - sonarqube-check

maven-deploy:
  << : *job_maven_config
  stage: deploy-package
  script: 
    - "JAR_FILE=$(mvn help:evaluate -Dexpression='project.build.finalName' -q -DforceStdout).jar"
    - "echo JAR_FILE=$JAR_FILE >> build.env"
    - "mvn $MAVEN_OPTS $MAVEN_CLI_OPTS deploy -s ci_settings.xml -Dmaven.test.skip=true -Pgitlab-cicd"
  artifacts:
    reports:
      dotenv: build.env
    paths:
      - ./target/*.jar
  dependencies:
    - maven-unit-test

kaniko-executor:
  image: 
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: deploy-image-container
  script:
    - "echo $JAR_FILE"
    - "echo $CI_REGISTRY_USER"
    - "echo $CONTAINER_SHA_IMAGE_TAG"
    # build image with kaniko
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(printf "%s:%s" "$CI_REGISTRY_USER" "$CI_REGISTRY_PASSWORD" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --build-arg "JAR_FILE=./target/$JAR_FILE"
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR/Dockerfile"
      --destination "$CONTAINER_SHA_IMAGE_TAG"
  rules:
    - !reference [.dev_rules, rules]
  environment: DEV
  dependencies:
    - maven-deploy

kubectl-apply-dev:
  image:
    name: $CI_REGISTRY/cehd/containers-images/kubectl-image/kubectl:1.0.0.RELEASE
    entrypoint: [""]
  stage: deploy-dev
  variables:
    NAMESPACE: $DEV_NAMESPACE
    HOST_NAME: $DEV_HOST_NAME
    TLS_SECRET_NAME: $DEV_TLS_SECRET_NAME
    IMAGE_TAG: "$CONTAINER_SHA_IMAGE_TAG"
  script:
    - "kubectl config use-context $DEV_KUBECTL_CONTEXT"
    - "envsubst < deployment.yaml > final-deployment.yaml"
    - "kubectl -n $NAMESPACE apply -f ./final-deployment.yaml"
  rules:
    - !reference [.dev_rules, rules]
  artifacts:
    paths: [./final-deployment.yaml]
    expire_in: 90d
  environment: DEV
  dependencies:
    - kaniko-executor

dastardly-dev:
  image: 
    name: public.ecr.aws/portswigger/dastardly:latest
    entrypoint: [""]
  stage: dast-dev
  variables:
    GIT_STRATEGY: none
    DASTARDLY_TARGET_URL: "https://$DEV_HOST_NAME/$DEPLOYMENT_NAME"
    DASTARDLY_OUTPUT_FILE: "$CI_PROJECT_NAME-dastardly-report.xml"
  script:
    - "/bin/bash /usr/local/bin/docker-entrypoint.sh dastardly"    
  rules:
    - !reference [.dev_rules, rules]
  artifacts:
    paths: ["$CI_PROJECT_NAME-dastardly-report.xml"]
    expire_in: 90d
  environment: DEV
  dependencies:
    - kubectl-apply-dev
  when: manual

clean-cache:
  stage: clean-cache
  << : *job_maven_config
  variables:
    GIT_STRATEGY: none
  script:
    - "ls -l $MAVEN_REPOSITORY"
    - "ls -l ./target"
    - "rm -rf $MAVEN_REPOSITORY"
    - "rm -rf ./target/"
  when: manual

# ------------------------------------------------------------------------
# QA
# ------------------------------------------------------------------------
retag-image:
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  stage: retag-image-container
  variables:
    GIT_STRATEGY: none
  script:
    - "echo $CONTAINER_SHA_IMAGE_TAG"
    - "crane auth login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    - "crane tag $CONTAINER_SHA_IMAGE_TAG $CI_COMMIT_TAG"
    - "crane tag $CONTAINER_SHA_IMAGE_TAG latest"
  rules:
    - !reference [.qa_rules, rules]
  environment: QA
  #dependencies:
  #  - kubectl-apply-dev

kubectl-apply-qa:
  image:
    name: $CI_REGISTRY/cehd/containers-images/kubectl-image/kubectl:1.0.0.RELEASE
    entrypoint: [""]
  stage: deploy-qa
  variables:
    NAMESPACE: $QA_NAMESPACE
    HOST_NAME: $QA_HOST_NAME
    TLS_SECRET_NAME: $QA_TLS_SECRET_NAME
    IMAGE_TAG: "$CONTAINER_IMAGE_TAG"
  script:
    - "kubectl config use-context $QA_KUBECTL_CONTEXT"
    - "envsubst < deployment.yaml > final-deployment.yaml"
    - "kubectl -n $NAMESPACE apply -f ./final-deployment.yaml"
  rules:
    - !reference [.qa_rules, rules]
  artifacts:
    paths: [./final-deployment.yaml]
    expire_in: 90d
  environment: QA
  dependencies:
    - retag-image

kubectl-undo-qa:
  image:
    name: $CI_REGISTRY/cehd/containers-images/kubectl-image/kubectl:1.0.0.RELEASE
    entrypoint: [""]
  stage: undo-qa
  variables:
    NAMESPACE: $QA_NAMESPACE
    GIT_STRATEGY: none
  script: 
    - "echo $DEPLOYMENT_NAME"
    - "kubectl config use-context $QA_KUBECTL_CONTEXT"
    - "kubectl -n $NAMESPACE rollout undo deployment/$DEPLOYMENT_NAME"
  when: manual
  rules:
    - !reference [.qa_rules, rules]
  environment: QA
  dependencies:
    - kubectl-apply-qa

# ------------------------------------------------------------------------
# PROD
# ------------------------------------------------------------------------
# kubectl-apply-prod:
#   image:
#     name: $CI_REGISTRY/containers-images/kubectl-image/kubectl:1.0.0.RELEASE
#     entrypoint: [""]
#   stage: deploy-prod
#   variables:
#     NAMESPACE: $PROD_NAMESPACE
#     HOST_NAME: $PROD_HOST_NAME
#     TLS_SECRET_NAME: $PROD_TLS_SECRET_NAME
#     IMAGE_TAG: "$CONTAINER_IMAGE_TAG"
#   script:
#     - "echo $ENVIRONMENT_NAME"
#     - "echo $IMAGE_TAG"
#     - "kubectl config use-context $PROD_KUBECTL_CONTEXT"
#     - "envsubst < deployment.yaml > final-deployment.yaml"
#     - "kubectl -n $NAMESPACE apply -f ./final-deployment.yaml"
#   rules:
#     - !reference [.prod_rules, rules]
#   artifacts:
#     paths: [./final-deployment.yaml]
#     expire_in: 90d
#   environment: PROD