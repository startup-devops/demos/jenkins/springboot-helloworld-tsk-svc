package com.cicd.tsk.svc.helloworld.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
class HelloWorldController {

	@PostMapping(path = "/")
    String[] home() {
    	ArrayList<String> buffer = new ArrayList<>();

    	buffer.add("Hello World! 1");
		buffer.add("Hello World! 2");
		//buffer.add("Hello World! 3");
		//buffer.add("Hello World! 4");
		//buffer.add("Hello World! 5");
    	String[] items = new String[buffer.size()];
    	return buffer.toArray(items);
    }

	void dumpCodeSmell() {
		// int i = 0;
		// Nothing
	}
}
